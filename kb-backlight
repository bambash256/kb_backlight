#!/usr/bin/python3

import types
import sys
import traceback

from keyboard_backlight_control import *

APP_NAME='kb-backlight'
REPO_LINK = 'https://gitlab.com/wmww/kb_backlight'
VERSION_X = 1
VERSION_Y = 0
VERSION_Z = 1

def parse_args(args):
    while len(args) > 0:
        i = args[0]
        if i == '-h' or i == '--help':
            print('usage: set-backlight [-v, --version] [-h, --help]')
            print('                     [-d, --debug] <command> [args]')
            print()
            print('options:')
            print('    -v, --version             display the version and exit')
            print()
            print('    -h, --help                display this help and exit')
            print()
            print('    -d, --debug               display what is being written and read from files')
            print()
            print('commands:')
            print('    brightness                print the current keyboard backlight brightness')
            print()
            print('    brightness {0-100}        set the keyboard backlight brightness to the')
            print('                              specified percentage, floating point values are')
            print('                              allowed')
            print()
            print('    color                     print the current keyboard backlight color')
            print()
            print('    color {000000-FFFFFF}...  set the keyboard backlight color to the specified')
            print('                              hex value; multiple colors can be specified if')
            print('                              your keyboard supports it')
            # 80:  ................................................................................
            return None
        if i == '-v' or i == '--version':
            print(APP_NAME + ' v' + str(VERSION_X) + '.' + str(VERSION_Y) + '.' + str(VERSION_Z))
            print('repo: ' + REPO_LINK)
            return None
        if i == '-d' or i == '--debug':
            set_debug(True)
        else:
            break
        args = args[1:]
    if len(args) == 0:
        raise KbError('please specify a command, or use -h for help')
    if args[0] == 'brightness':
        if len(args) < 2:
            def func(factory):
                x = factory.brightness_ctrl().get()
                print(str(x * 100) + '%')
            return func
        elif len(args) == 2:
            value = None
            try:
                value = float(args[1]) / 100.0
            except ValueError as e:
                raise KbError('\'' + args[1] + '\' is not a valid number')
            if value < 0 or value > 1:
                new = max(min(value, 1), 0)
                print('WARNING: brightness ' + args[1] + ' is not in the range 0 - 100, clamping to ' + str(int(new * 100)), file=sys.stderr)
                value = new
            def func(factory):
                factory.brightness_ctrl().set(value)
            return func
        else:
            raise KbError('too many argumants to \'brightness\' command, use -h for help')
    if args[0] == 'color':
        if len(args) < 2:
            def func(factory):
                x = factory.color_ctrl().get_list()
                print(' '.join(c.get_hex() for c in x))
            return func
        elif len(args) == 2:
            try:
                clr = Color(args[1])
                def func(factory):
                    factory.color_ctrl().set_single(clr)
                return func
            except AssertionError as e:
                raise KbError(args[1] + ' is not a valid hex color because ' + str(e))
        else:
            l = []
            for i in args[1:]:
                try:
                    l.append(Color(i))
                except AssertionError as e:
                    raise KbError(i + ' is not a valid hex color because ' + str(e))
            def func(factory):
                num = factory.color_ctrl().get_num()
                if len(l) != num:
                    raise KbError('keyboard has ' + str(num) + ' backlight colors, not ' + str(len(l)))
                factory.color_ctrl().set_list(l)
            return func
    else:
        raise KbError('unknown command \'' + args[0] + '\', use -h for help')
    raise AssertionError('parse_args() failed to return anything')

if __name__ == '__main__':
    try:
        cmd = parse_args(sys.argv[1:])
        if cmd != None:
            factory = Factory()
            cmd(factory)
    except KbError as e:
        print(str(e), file=sys.stderr)
        exit(1)
    except Exception as e:
        print(APP_NAME + ' version ' + str(VERSION_X) + '.' + str(VERSION_Y) + '.' + str(VERSION_Z), file=sys.stderr)
        print()
        traceback.print_exc(file=sys.stderr)
        print()
        print('please report this issue at ' + REPO_LINK + '/issues', file=sys.stderr)
        exit(1)
